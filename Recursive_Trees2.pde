float xPos;
float yPos;
int iterationCounter;
float theta;
float thetaOffset;
final int MINIMUM_BRANCH_LENGTH = 5;
final int INIT_BRANCH_LENGTH = 120;

void setup(){
  fullScreen();
  //size(1024, 768);
  frameRate(1);
  background(0);
  stroke(255);
  strokeWeight(1);
  colorMode(HSB);
  xPos = width /2;
  yPos = height - 100;
  theta = -90.0;
}

void draw(){
  background(0);
  //thetaOffset = random(0, 30); //map(mouseX, 0, width, 0, 30);
  drawQuaverTree(INIT_BRANCH_LENGTH, xPos, yPos, theta);
  noLoop();
}

void mouseClicked(){
  redraw();
}

void drawQuaverTree(float len, float x, float y, float theta){
  iterationCounter++;
  //base case
  if(len < MINIMUM_BRANCH_LENGTH){
    return;
  }
  float newX = calculateXCoordinate(x, len, theta);
  float newY = calculateYCoordinate(y, len, theta);
  float strokeThickness = map(len, 0, INIT_BRANCH_LENGTH, 1, 9);
  stroke(len, 255, 255);
  strokeWeight(strokeThickness);
  line(x, y, newX, newY);
 
  drawQuaverTree(len * getRandomShorteningFactor(), newX, newY, theta + getRandomThetaOffset());
  drawQuaverTree(len * getRandomShorteningFactor(), newX, newY, theta - getRandomThetaOffset());
  
  //here we have a 33% chance of a third limb at a junction
  int thirdLimb = (int) random(0,3);
  if(thirdLimb < 1){
    //if flipFlop is 0 we apply a positive random theta, otherwise it's negative. 
    int flipFlop = (int) random(0,2);
    if(flipFlop ==0){
       drawQuaverTree(len * getRandomShorteningFactor(), newX, newY, theta + getRandomThetaOffset());
    }else{
       drawQuaverTree(len * getRandomShorteningFactor(), newX, newY, theta - getRandomThetaOffset());

    }
  }
}

private float getRandomThetaOffset(){
    return random(0, 30);
}

// final parameter is key, a value of 1 could create an infinite or near infinite loop
private float getRandomShorteningFactor(){
  return map(random(0,1), 0, 1, 0.60, 0.90);
}

//@param angle: the angle in degrees
public float calculateXCoordinate(float startX, float length, float angle){
 float x = startX + length * cos(radians(angle));
 return x;
}

//@param angle: the angle in degrees
public float calculateYCoordinate(float startY, float length, float angle){
  float y = startY + length * sin(radians(angle));
  return y;
}